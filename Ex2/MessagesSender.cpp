#include "MessagesSender.h"
#include <mutex>
#include <fstream>

std::mutex mutxMsg;
std::mutex mutxUsers;
std::ifstream infile;
std::ofstream outfile;
std::condition_variable cond;

void clear_file(std::string s) 
{
	std::ofstream f;
	f.open(s);
	f << "";
}

MessagesSender::MessagesSender() : _userNames(), _msgs()
{
	
}

MessagesSender::~MessagesSender() 
{

}

void MessagesSender::menu()
{
	int choice;
	do
	{
		printMenu();
		std::cin >> choice;
		switch (choice) {
		case 1:
			this->signIn();
			break;
		case 2:
			this->signOut();
			break;
		case 3:
			this->printAllUsers();
			break;
		case 4:
			break;
		default:
			std::cout << "Only 1-4 input" << std::endl;
			break;
		}
	} while (choice != 4);
}

void MessagesSender::printMenu() 
{
	std::cout << "1. Sign in\n2. Sign out\n3. Print all connected users\n4. Exit" << std::endl;
}

void MessagesSender::signIn() 
{
	std::string name;
	std::cout << "Enter your user name: ";
	std::cin >> name;
	std::unique_lock<std::mutex> locker(mutxUsers);
	if (_userNames.find(name) != _userNames.end()) 
		std::cout << name << " is alredy connected user" << std::endl;
	else 
	{
		_userNames.insert(name);
		std::cout << "connect succeeded" << std::endl;
	}
}

void MessagesSender::signOut()
{
	std::string name;
	std::cout << "Enter the user name to sign out: ";
	std::cin >> name;
	std::unique_lock<std::mutex> locker(mutxUsers);
	if (_userNames.find(name) != _userNames.end())
	{
		_userNames.erase(name);
		std::cout << "sign out succeeded" << std::endl;
	}
	else 
		std::cout << name << " isnt connected user" << std::endl;
}

void MessagesSender::printAllUsers() 
{
	std::cout << "All the connected users are:" << std::endl;
	std::set<std::string>::iterator it;
	for (it = _userNames.begin(); it != _userNames.end(); it++) 
		std::cout << *it << std::endl;
	std::cout << std::endl;
}

void MessagesSender::readMassegeFromFile() 
{
	std::string line;
	bool b = false;
	while (true)
	{
		if (!infile.is_open())
			infile.open("data.txt");
		while (!(infile.peek() == std::ifstream::traits_type::eof()))
		{
			std::unique_lock<std::mutex> locker(mutxMsg);
			std::getline(infile, line);
			_msgs.push(line);
			b = true;
		}
		if (b) 
		{
			infile.close();
			clear_file("data.txt");
			cond.notify_one();
			b = false;
		}
		std::this_thread::sleep_for(std::chrono::seconds(60));
	}
}

void MessagesSender::sendMassege() {
	if (!outfile.is_open()) {
		outfile.open("output.txt", std::fstream::app);
	}
	while (true) {
		std::unique_lock<std::mutex> locker(mutxMsg);
		cond.wait(locker);
		while (!_msgs.empty()) {
			std::set<std::string>::iterator it;
			std::unique_lock<std::mutex> locker(mutxUsers);
			std::string msg = _msgs.front();
			_msgs.pop();
			for (it = _userNames.begin(); it != _userNames.end(); it++) {
				outfile << *it << ": " << msg << std::endl;
			}

		}
	}
}


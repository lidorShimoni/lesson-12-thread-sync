#include <string>
#include <fstream>
#include <iostream>
#include <set>
#include <mutex>
#include <queue>

class MessagesSender {
public:
	MessagesSender();
	~MessagesSender();
	void menu();
	void printMenu();
	void signIn();
	void signOut();
	void printAllUsers();
	void readMassegeFromFile();
	void sendMassege();
private:
	std::set<std::string> _userNames;
	std::queue<std::string> _msgs;
};
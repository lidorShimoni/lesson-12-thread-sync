#include "threads.h"

int main()
{
	//c
	callGetPrimes(0, 1000);
	callGetPrimes(0, 100000);
	//e
	callWritePrimesMultipleThreads(0, 1000, "1.txt", 5);
	callWritePrimesMultipleThreads(0, 100000, "2.txt", 5);
	system("pause");
	return 0;
}
#include "threads.h"
#include <thread>
#include <mutex>

mutex mutx;
ofstream file;
once_flag  f;

void I_Love_Threads() {
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads() {
	std::thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes) {
	std::vector<int>::iterator it;
	for (it = primes.begin(); it != primes.end(); it++) {
		std::cout << *it << std::endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes) {
	bool isPrime;
	for (int i = begin; i <= end; i++) {
		isPrime = true;
		for (int j = 2; j < i && isPrime; j++) {
			if (i%j == 0)
				isPrime = false;
		}
		if (isPrime)
			primes.push_back(i);
	}
}

vector<int> callGetPrimes(int begin, int end) {
	double t = clock();
	vector<int> v;
	std::thread t1(getPrimes,begin, end, std::ref(v));
	t1.join();
	double t2 = clock();
	std::cout << "runing time: " << t2 - t / double(CLOCKS_PER_SEC) * 1000  << " milisec" << std::endl;
	return v;
}

void writePrimesToFile(int begin, int end, ofstream& file) {
	bool isPrime;
	for (int i = begin; i <= end; i++) {
		isPrime = true;
		for (int j = 2; j < i && isPrime; j++) {
			if (i%j == 0)
				isPrime = false;
		}
		std::unique_lock<mutex> locker(mutx);
		if (isPrime)
			file << i << "\n";
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N) {
	double t = clock();
	std::call_once(f, [&]() {file.open(filePath); });
	int x = (end - begin) / N;
	for (int i = 0; i < N; i++) {
		std::thread t1(writePrimesToFile,begin + i*x, begin + (i+1)*x, std::ref(file));
		t1.detach();
	}
	std::thread t1(writePrimesToFile, begin + (N-1)*x, end, std::ref(file));
	t1.join();
	double t2 = clock();
	std::cout << "runing time: "  << t2 - t  / double(CLOCKS_PER_SEC) * 1000 << " milisec" << std::endl;
}
